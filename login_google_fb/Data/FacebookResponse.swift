//
//  FacebookResponse.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 22/01/21.
//

import Foundation
class FacebookResponse: Codable {

var id: Int?
var email: String?
var first_name: String?
var last_name: String?
var name: String?
var picture: PictureFacebookResponse?
    
    init(response: [String: Any]?) {
      
        guard let response = response else {
            return
        }
        
        self.id = response["id"] as? Int
        self.email = response["email"] as? String
        self.first_name = response["first_name"] as? String
        self.name = response["name"] as? String
        self.picture = PictureFacebookResponse.init(response: response["picture"] as? [String : Any])
        
    }
    
}


class PictureFacebookResponse: Codable {
var data: DataFacebookResponse?
    
    init(response: [String: Any]?) {
      
        guard let response = response else {
            return
        }
        
        self.data = DataFacebookResponse.init(response: response["data"] as? [String : Any])
        
    }
    
}


class DataFacebookResponse: Codable {
    var height : Int?
    var is_silhouette : Int?
    var url : String?
    var width : Int?
    
    init(response: [String: Any]?) {
         
           guard let response = response else {
               return
           }
           
        self.height = response["height"] as? Int
        self.is_silhouette = response["is_silhouette"] as? Int
        self.url = response["url"] as? String
        self.width = response["width"] as? Int
           
       }
}
