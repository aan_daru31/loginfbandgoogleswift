//
//  HomeResponse.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import Foundation
class HomeResponse: Codable {
var data    : DataHomeResponse?
    
    init(response:[String:Any]?) {
        guard let response = response else {
            return
        }
        self.data = DataHomeResponse.init(response: response["data"] as? [String : Any])
      }
}

class DataHomeResponse: Codable {
    var categoryResponse    : [CategoryResponse]?
    var productPromo : [ProductPromoResponse]?
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        
        var itemsCategory : [CategoryResponse] = []
        let dataCategory = response["category"] as! [Any]
        for category in dataCategory {
            let itemCategory = CategoryResponse.init(response: category as? [String : Any])
            itemsCategory.append(itemCategory)
        }
        
        
        var itemsProductPromo: [ProductPromoResponse] = []
        let dataProductPromo = response["productPromo"] as! [Any]
        for category in dataProductPromo {
            let itemProductPromo = ProductPromoResponse.init(response: category as? [String : Any])
            itemsProductPromo.append(itemProductPromo)
        }
        
        
        self.categoryResponse = itemsCategory
        self.productPromo = itemsProductPromo
             
      }
}

class CategoryResponse: Codable {
    var id : Int?
    var imageUrl : String?
    var name : String?
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        self.id = response["id"] as? Int
        self.imageUrl = response["imageUrl"] as? String
        self.name = response["name"] as? String
             
      }
}

class ProductPromoResponse: Codable {
    var id : String?
    var imageUrl : String?
    var title : String?
    var description : String?
    var price : String?
    var loved : Int?
    
    init(id:String?) {
        self.id = id
    }
    
    init(id : String?, imageUrl : String?, title : String?, description : String?, price : String?, loved : Int?) {
        self.id = id
        self.imageUrl = imageUrl
        self.title = title
        self.description = description
        self.price = price
        self.loved = loved
    }
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        self.id = response["id"] as? String
        self.imageUrl = response["imageUrl"] as? String
        self.title = response["title"] as? String
        self.description = response["description"] as? String
        self.price = response["price"] as? String
        self.loved = response["loved"] as? Int
             
      }
}
