//
//  History.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

struct History{
    let id: String
    let imageUrl: String
    let title: String
    let description: String
    let price: String
    let loved: Int
}
