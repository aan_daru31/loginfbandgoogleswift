//
//  VMDetil.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 22/01/21.
//

import RxSwift

final class VMDetil{
    
    // fungsi tambah data
    func save(_ productPromoResponse: ProductPromoResponse){
        
        var productPromoResponses: [ProductPromoResponse] = []
        let dataOld = getData()
        if (dataOld.count != 0){
            productPromoResponses = productPromoResponses + dataOld
            productPromoResponses.append(productPromoResponse)
        } else {
            productPromoResponses.append(productPromoResponse)
        }
        
        if let data = try? JSONEncoder().encode(productPromoResponses) {
            let defaults = UserDefaults.standard
            
            
            defaults.set(data, forKey: defaultsKeys.history)
            
        }
        
        
        }
        
        // fungsi refrieve semua data
        func getData() -> [ProductPromoResponse]{
            
            let data = UserDefaults.standard.data(forKey: defaultsKeys.history)
            let productPromoResponse = data != nil ? try? JSONDecoder().decode([ProductPromoResponse].self, from: data!) : nil
            
            
            return productPromoResponse ?? []
            
        }
}

struct defaultsKeys {
    static let history = "history"
}

