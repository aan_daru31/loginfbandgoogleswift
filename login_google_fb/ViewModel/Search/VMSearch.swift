//
//  VMSearch.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import RxSwift

final class VMSearch{
    let filter = PublishSubject<String>()
    var productPromoResponseArray : [ProductPromoResponse] = []
}
