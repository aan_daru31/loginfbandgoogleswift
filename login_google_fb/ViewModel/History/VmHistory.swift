//
//  VmHistory.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import Foundation
final class VmHistory{
    
        
        // fungsi refrieve semua data
        func getData() -> [ProductPromoResponse]{
            
            let data = UserDefaults.standard.data(forKey: defaultsKeys.history)
            let productPromoResponse = data != nil ? try? JSONDecoder().decode([ProductPromoResponse].self, from: data!) : nil
            
            
            return productPromoResponse ?? []
            
        }
}
