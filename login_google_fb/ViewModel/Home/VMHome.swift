//
//  VMHome.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import Foundation
import Alamofire
import RxSwift
import SwiftyJSON

final class VMHome{
    let homeResponses = PublishSubject<HomeResponse>()
    
    func fetchData(){
        let url = "https://private-4639ce-ecommerce56.apiary-mock.com/home"
        
        AF.request( url).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                guard let jsonArray = value as? [[String: Any]] else {
                    return
                }
                let homeResponse = HomeResponse(response:  jsonArray[0])
                self.homeResponses.onNext(homeResponse)
                
                
            case .failure(let error):
                print("\n Failure: \(error.localizedDescription)")
            }
        }
    }
    
    func getData() -> [ProductPromoResponse]{
        
        let data = UserDefaults.standard.data(forKey: defaultsKeys.history)
        let productPromoResponse = data != nil ? try? JSONDecoder().decode([ProductPromoResponse].self, from: data!) : nil
        
        
        return productPromoResponse ?? []
        
    }
    
}
