//
//  CategoriViewCell.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import UIKit
import RxSwift

class CategoriViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var categoryResponse : [CategoryResponse] = []
   
    let homeResponses = PublishSubject<[CategoryResponse]>()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        homeResponses.subscribe { (response)
            in
            self.categoryResponse = (response.element!)
            self.setList()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//MARK: - Setup Table
extension CategoriViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func setList(){
        self.collectionView.delegate   = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryResponse.count
        }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        self.collectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as? CategoryCollectionViewCell
        cell?.lblName.text = categoryResponse[indexPath.row].name
        cell?.imageView.load(url: URL(string: categoryResponse[indexPath.row].imageUrl!)!)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: (collectionView.layer.bounds.width / 5.2), height: 112)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        menuActive = indexPath.row
//        collectionView.reloadData()
     }
    
    
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
