//
//  DetilViewCell.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import UIKit
import RxSwift

class DetilViewCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgLove: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    let backButton = PublishSubject<Bool>()
    private var productPromoResponse : ProductPromoResponse?
    private var selfs : UIViewController?
    private var viewModel: VMDetil?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(productPromoResponse : ProductPromoResponse, selfs : UIViewController, viewModel: VMDetil){
        self.viewModel = viewModel
        self.productPromoResponse = productPromoResponse
        self.selfs = selfs
        img.load(url: URL(string: productPromoResponse.imageUrl!)!)
        lblTitle.text = productPromoResponse.title
        lblContent.text = productPromoResponse.description
        lblPrice.text = productPromoResponse.price
        imgLove.image = UIImage(named: productPromoResponse.loved == 1 ? "ic_heart_black" : "ic_heart_blank")
    }
    
    
    @IBAction func share(_ sender: Any) {
        alert(title : "Share", message : productPromoResponse?.description ?? "")
    }
    
    
    @IBAction func btnBuy(_ sender: Any) {
        self.viewModel?.save(productPromoResponse!)
        alert(title : "Yey", message : "Berhasil Tersimpan Di History")
    }
    @IBAction func btnBack(_ sender: Any) {
        backButton.onNext(true)
    }
    
    private func alert(title : String, message : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        selfs?.present(alert, animated: true, completion: nil)
    }
}
