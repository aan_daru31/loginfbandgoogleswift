//
//  VCSearch.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import UIKit

class VCSearch: UIViewController {
    @IBOutlet weak var txfSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var viewModel : VMSearch?
    
    var productPromoResponseArray : [ProductPromoResponse] = []
    var productPromoResponseFilterArray : [ProductPromoResponse] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txfSearch.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        
        viewModel?.filter.subscribe{(response) in
            
            if response.element! != "" {
                self.productPromoResponseFilterArray = (self.productPromoResponseArray.filter{ $0.title!.localizedStandardContains(response.element!)})
            } else {
                self.productPromoResponseFilterArray = []
                self.tableView.reloadData()
            }
            
            self.setupUITable()
        }
        
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        viewModel?.filter.onNext(sender.text ?? "")
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - Setup Table
extension VCSearch: UITableViewDataSource, UITableViewDelegate {
    
    
    private func setupUITable(){
        self.tableView.dataSource = self
        tableView.delegate = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productPromoResponseFilterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            let nibSearchViewCell = UINib(nibName: "SearchViewCell", bundle: nil)
            self.tableView.register(nibSearchViewCell, forCellReuseIdentifier: "SearchViewCell")
            let searchViewCell = tableView.dequeueReusableCell(withIdentifier: "SearchViewCell", for: indexPath) as! SearchViewCell
        searchViewCell.img.load(url: URL(string: productPromoResponseFilterArray[indexPath.row].imageUrl!)!)
            
        searchViewCell.lblName.text = productPromoResponseFilterArray[indexPath.row].title
        searchViewCell.price.text = productPromoResponseFilterArray[indexPath.row].price
            return  searchViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCDetil") as! VCDetil
        popOverVC.productPromoResponse = productPromoResponseFilterArray[indexPath.row]
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
    }
    
}
