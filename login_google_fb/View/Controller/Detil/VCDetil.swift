//
//  VCDetil.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import UIKit

class VCDetil: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var productPromoResponse : ProductPromoResponse?
    var viewModel: VMDetil?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUITable()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Setup Table
extension VCDetil: UITableViewDataSource, UITableViewDelegate {
    
    
    private func setupUITable(){
        self.tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.bounds.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let nibDetilViewCell = UINib(nibName: "DetilViewCell", bundle: nil)
            self.tableView.register(nibDetilViewCell, forCellReuseIdentifier: "DetilViewCell")
            var detilViewCell = tableView.dequeueReusableCell(withIdentifier: "DetilViewCell", for: indexPath) as! DetilViewCell
        detilViewCell.setData(productPromoResponse : productPromoResponse!, selfs: self, viewModel: viewModel!)
        detilViewCell.backButton.subscribe { (response)
            in
            self.dismiss(animated: true, completion: nil)
        }
            return detilViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let data = dataCity[indexPath.row]
    }
    
}
