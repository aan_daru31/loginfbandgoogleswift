//
//  ViewController.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import UIKit
import FBSDKLoginKit
import FacebookLogin
import FBSDKCoreKit
import GoogleSignIn
import FacebookCore

class ViewController: UIViewController, GIDSignInDelegate {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var txfUserName: UITextField!
    @IBOutlet weak var txfPassword: UITextField!
    @IBOutlet weak var btnSign: UIButton!
    @IBOutlet weak var imvCheckBox: UIImageView!
    @IBOutlet weak var viewSignFacebook: UIView!
    @IBOutlet weak var viewSignGoogle: UIView!
    
    private var isCheckBox = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
    }
    
    
    private func initView(){
        
        viewContent.layer.cornerRadius = 8.0
        viewContent.layer.borderWidth = 0.5
        viewContent.layer.borderColor = UIColor.systemGray.cgColor
        btnSign.layer.cornerRadius = 8.0
        viewSignFacebook.layer.cornerRadius = 8.0
        viewSignGoogle.layer.cornerRadius = 8.0
        GIDSignIn.sharedInstance().clientID = "955702573352-ijo54af3s2qtpmsk43el08llbp8dfes1.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
    }
    
    

    @IBAction func btnCheckBox(_ sender: Any) {
        
        isCheckBox = !isCheckBox
        imvCheckBox.image = UIImage(named: isCheckBox ? "ic_check_box" : "ic_check_blank")
        
    }
    
    @IBAction func btnSign(_ sender: Any) {
        signIn()
    }
    
    private func signIn(){
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCHome") as! VCHome
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
    }
    
    @IBAction func btnFacebook(_ sender: Any) {
        
        let loginManager = LoginManager()
        
        loginManager.logOut()
        AccessToken.current = nil
        
        if let _ = AccessToken.current {
            
            UserDefaults.standard.set(AccessToken.current?.tokenString, forKey: "fbToken")

            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    let dictionary = result as! NSDictionary
                    _ = FacebookResponse.init(response: (dictionary as! [String : Any]))

                    URLCache.shared.removeAllCachedResponses()
                        URLCache.shared.diskCapacity = 0
                        URLCache.shared.memoryCapacity = 0

                        if let cookies = HTTPCookieStorage.shared.cookies {
                            for cookie in cookies {
                                HTTPCookieStorage.shared.deleteCookie(cookie)
                            }
                        }
                    
                    self.signIn()
                }
            })
            
        } else {
        
            loginManager.logIn(permissions: ["email"], from: self) { (loginResult, error) in
               
                
                if error != nil
                {
                    //Error
                }
                else
                {
                    if loginResult?.grantedPermissions == nil
                    {
                        //Login Permissions not granted
                        return
                    }
                    
                    
                    
                    if (loginResult?.grantedPermissions.contains("email"))!
                    {
                        UserDefaults.standard.set(AccessToken.current?.tokenString, forKey: "fbToken")
                        
                        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                            if (error == nil) {
                                let dictionary = result as! NSDictionary
                                let facebookResponse = FacebookResponse.init(response: (dictionary as! [String : Any]))
                                
                                URLCache.shared.removeAllCachedResponses()
                                    URLCache.shared.diskCapacity = 0
                                    URLCache.shared.memoryCapacity = 0

                                    if let cookies = HTTPCookieStorage.shared.cookies {
                                        for cookie in cookies {
                                            HTTPCookieStorage.shared.deleteCookie(cookie)
                                        }
                                    }
                                self.signIn()
                                
                            }
                        })
                    }
                }
            }
            
        }
        
    }
    @IBAction func btnGoogle(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
         GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    // [START signin_handler]
       func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                 withError error: Error!) {
         if let error = error {
           if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//            Utils.visibleDialogWarning(selfView: self, message: Strings.USER_HAS_NOT_SIGNED, titles: Strings.TITLE_DIALOG_GENERAL)
           } else {
             print("\(error.localizedDescription)")
           }
           // [START_EXCLUDE silent]
           NotificationCenter.default.post(
             name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
           // [END_EXCLUDE]
           return
         } else {
            self.signIn()
//
            // [START_EXCLUDE]
            NotificationCenter.default.post(
              name: Notification.Name(rawValue: "ToggleAuthUINotification"),
              object: nil,
              userInfo: ["statusText": "Signed in user:\n\(user.profile.name!)"])
            // [END_EXCLUDE]
            
        }
         
       }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // [START_EXCLUDE]
      NotificationCenter.default.post(
        name: Notification.Name(rawValue: "ToggleAuthUINotification"),
        object: nil,
        userInfo: ["statusText": "User has disconnected."])
      // [END_EXCLUDE]
    }
    
    
}

