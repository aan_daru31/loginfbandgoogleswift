//
//  VCHistory.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import UIKit

class VCHistory: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var productPromoResponseArray : [ProductPromoResponse] = []
    var viewModel : VmHistory?
    override func viewDidLoad() {
        super.viewDidLoad()
        productPromoResponseArray = viewModel?.getData() ?? []
        
        setupUITable()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Setup Table
extension VCHistory: UITableViewDataSource, UITableViewDelegate {
    
    
    private func setupUITable(){
        self.tableView.dataSource = self
        tableView.delegate = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productPromoResponseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            let nibSearchViewCell = UINib(nibName: "SearchViewCell", bundle: nil)
            self.tableView.register(nibSearchViewCell, forCellReuseIdentifier: "SearchViewCell")
            let searchViewCell = tableView.dequeueReusableCell(withIdentifier: "SearchViewCell", for: indexPath) as! SearchViewCell
        searchViewCell.img.load(url: URL(string: productPromoResponseArray[indexPath.row].imageUrl!)!)
            
        searchViewCell.lblName.text = productPromoResponseArray[indexPath.row].title
        searchViewCell.price.text = productPromoResponseArray[indexPath.row].price
            return  searchViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = VMDetil.init()
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCDetil") as! VCDetil
        popOverVC.viewModel = viewModel
        popOverVC.productPromoResponse = productPromoResponseArray[indexPath.row]
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
    }
    
}
