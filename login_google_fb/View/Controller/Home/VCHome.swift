//
//  VCHome.swift
//  login_google_fb
//
//  Created by Akhmad Andaru on 17/01/21.
//

import Foundation
import UIKit

class VCHome: UIViewController {
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var cvMenu: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    private var isLove = false
    private var menu = ["Home", "Feed", "Cart", "Profile"]
    private var menuActive = 0
    private var categoryResponse : [CategoryResponse] = []
    private var productPromoResponse : ProductPromoResponse?
    private var productPromoResponseArray : [ProductPromoResponse] = []
    private var productPromoHistory : [ProductPromoResponse] = []
    var viewModel =  VMHome.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
        viewModel.fetchData()
        
        productPromoHistory = viewModel.getData()
        
    }
    
    private func initView(){
        viewSearch.layer.cornerRadius = 8.0
        viewSearch.layer.borderWidth = 0.5
        viewSearch.layer.borderColor = UIColor.systemGray.cgColor
        
        viewModel.homeResponses.subscribe { (response)
            in
            self.categoryResponse = (response.element!.data?.categoryResponse)!
            
            self.productPromoResponse = ProductPromoResponse.init(id: "0")
            
            self.productPromoResponseArray.append(self.productPromoResponse!)
            
            self.productPromoResponseArray =  self.productPromoResponseArray + (response.element!.data?.productPromo)!
            self.setupUITable()
        }
        setMenu()
        
    }
    
    
    @IBAction func btnLove(_ sender: Any) {
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        
        let viewModelSearch = VMSearch.init()
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCSearch") as! VCSearch
        popOverVC.productPromoResponseArray = productPromoResponseArray.filter { $0.id != "0" }
        popOverVC.viewModel = viewModelSearch
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
    }
    
}

//MARK: - Setup Table
extension VCHome: UITableViewDataSource, UITableViewDelegate {
    
    
    private func setupUITable(){
        self.tableView.dataSource = self
        tableView.delegate = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 150 : 280
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productPromoResponseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ( indexPath.row == 0 ){
            let nibCategoriViewCell = UINib(nibName: "CategoriViewCell", bundle: nil)
            self.tableView.register(nibCategoriViewCell, forCellReuseIdentifier: "CategoriViewCell")
            var categoriViewCell = tableView.dequeueReusableCell(withIdentifier: "CategoriViewCell", for: indexPath) as! CategoriViewCell
            categoriViewCell.homeResponses.onNext(categoryResponse)
            
            return categoriViewCell
            
        } else {
            let nibProductViewCell = UINib(nibName: "ProductViewCell", bundle: nil)
            self.tableView.register(nibProductViewCell, forCellReuseIdentifier: "ProductViewCell")
            let productViewCell = tableView.dequeueReusableCell(withIdentifier: "ProductViewCell", for: indexPath) as! ProductViewCell
            productViewCell.img.load(url: URL(string: productPromoResponseArray[indexPath.row].imageUrl!)!)
            
            productViewCell.lblName.text = productPromoResponseArray[indexPath.row].title
            
            productViewCell.img_heart.image = UIImage(named: productPromoResponseArray[indexPath.row].loved == 1 ? "ic_heart_black" : "ic_heart_blank")
            return  productViewCell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row != 0){
            goToDetil(row : indexPath.row)
            
        }
        
    }
    
    private func goToDetil(row : Int){
        let viewModel = VMDetil.init()
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCDetil") as! VCDetil
        popOverVC.viewModel = viewModel
        popOverVC.productPromoResponse = menuActive == 0 ? productPromoResponseArray[row] : productPromoHistory[row]
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
    }
    
    
}

//MARK: - Setup Table
extension VCHome: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func setMenu(){
        self.cvMenu.delegate   = self
        self.cvMenu.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
        }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        self.cvMenu.register(UINib(nibName: "MenuBottomViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuBottomViewCell")
        let cell = cvMenu.dequeueReusableCell(withReuseIdentifier: "MenuBottomViewCell", for: indexPath) as? MenuBottomViewCell
        cell?.lblName.text = menu[indexPath.row]
        cell?.lblName.textColor = menuActive == indexPath.row ? UIColor.blue : UIColor.black
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: (cvMenu.layer.bounds.width / 5.2), height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row != 3){
            menuActive = indexPath.row
            cvMenu.reloadData()
            tableView.reloadData()
        } else if (indexPath.row == 3) {
            var viewModel = VmHistory.init()
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCHistory") as! VCHistory
            popOverVC.viewModel = viewModel
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            popOverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(popOverVC, animated: true, completion: nil)
        }
       
     }
    
    
}
